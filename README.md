# Setup for Debian operated mirrors

## Playbooks

### site.playbook

Sets up archvsync (ftpsync, runmirrors), rsync secrets and ssh ``authorized_keys`` on all managed mirrors.

### mirrorinfo.playbook

Create a sheet with information for each external mirror including password, hosts to pull from, ssh ``authorized_keys`` and IP of the upstreams.
