from __future__ import absolute_import, division, print_function

import base64

from ansible.errors import AnsibleError


def ssh_push_key(hostname, hostvars, results):
    try:
        return hostvars[hostname]['ssh_push_key']

    except KeyError:
        pass

    for r in results:
        if r['item'] == hostname:
            return base64.b64decode(r['content']).strip()

    raise AnsibleError


class FilterModule(object):
    def filters(self):
        return {
            'ssh_push_key': ssh_push_key,
        }
