from __future__ import absolute_import, division, print_function

from ansible.errors import AnsibleError


def gather_downstream(hostname, hostvars, groups, group_names):
    ret = {}

    for host in groups['all']:
        config = hostvars[host].get('mirror_config', {})
        for name, mirror in config.iteritems():
            name = mirror.get('path', name)
            upstream = mirror.get('upstream')
            if upstream == hostname or upstream in group_names:
                ret.setdefault(name, set()).add(host)

    return { k: list(v) for k, v in ret.iteritems() }

# Collect all manually added hosts
def gather_runmirrors_manual(config, hostvars):
    ret = {}

    def config_push(name, dname):
        r = (host in hostvars and hostvars[host].get('mirror_{}'.format(name)))
        if r:
            d.setdefault(dname, r)

    for name, mirror in config.iteritems():
        r = ret.setdefault(name, {})
        for d in mirror.get('runmirrors', ()):
            if 'host' in d:
                host = d['host']
                config_push('upstream_push_type', 'type')
                config_push('upstream_push_url', 'url')
                config_push('upstream_push_user', 'user')
                r[host] = d

    return ret

# Collect all hosts wanting to get triggered
def gather_runmirrors_want(hostname, hostvars, groups, group_names):
    ret = {}

    def config_get(name, default=None):
        return (mirror.get(name) or
                hostvars[host].get('mirror_{}'.format(name)) or
                default)

    def config_push(name, dname, default=None):
        r = config_get(name, default)
        if r is not None:
            d.setdefault(dname, r)

    for host in groups['all']:
        c = hostvars[host].get('mirror_config', {})

        for name, mirror in c.iteritems():
            upstream = mirror.get('upstream')
            name = mirror.get('upstream_name', name)
            push = config_get('upstream_push')

            if ((upstream == hostname or upstream in group_names) and push):
                d = {}

                config_push('upstream_push_delay', 'delay',
                        host in groups['debian'] and -300 or 0)
                config_push('upstream_push_host', 'host', host)
                config_push('upstream_push_name', 'name')
                config_push('upstream_push_type', 'type')
                config_push('upstream_push_url', 'url')
                config_push('upstream_push_user', 'user')

                ret.setdefault(name, {})[host] = d

    return ret

def gather_runmirrors(hostname, hostvars, groups, group_names):
    config = hostvars[hostname].get('mirror_config', {})

    runmirrors_manual = gather_runmirrors_manual(config, hostvars)
    runmirrors_want = gather_runmirrors_want(hostname, hostvars, groups, group_names)

    # Merge both sets
    for name, manual in runmirrors_manual.items():
        want = runmirrors_want.get(name, [])

        add = {}
        for host in sorted(frozenset(want) - frozenset(manual)):
            delay = want[host]['delay']
            add.setdefault(delay, []).append(want[host])

        delay_last = None
        h = []
        for delay, entries in sorted(add.items()):
            if delay_last:
                h.append({'special': 'DELAY {}'.format(delay - delay_last)})
            delay_last = delay
            h.extend(entries)
        config[name].setdefault('runmirrors', [])[0:0] = h

    return config


def gather_upstream(hostname, hostvars, groups):
    ret = set()

    config = hostvars[hostname].get('mirror_config', {})
    for name, mirror in config.iteritems():
        upstream = mirror.get('upstream')
        if not upstream:
            continue
        if upstream in groups:
            ret.update(groups[upstream])
        elif upstream in groups['all']:
            ret.add(upstream)
        else:
            raise AnsibleError('Unable to find upstream {} for {}:{}'
                .format(upstream, hostname, name))

    return list(ret)


def name(key):
    if key.startswith('debian'):
        return key[6:]
    return '-' + key


class FilterModule(object):
    def filters(self):
        return {
            'mirror_config_gather_downstream': gather_downstream,
            'mirror_config_gather_runmirrors': gather_runmirrors,
            'mirror_config_gather_upstream': gather_upstream,
            'mirror_config_name': name,
        }
